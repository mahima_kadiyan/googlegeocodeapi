var _ = require("lodash");
const Client = require("@googlemaps/google-maps-services-js").Client;
const client = new Client({});
const getCompleteAddress = (dbData: any) => {
  let address = [];
  if (!_.isEmpty(dbData.primaryAddress)) {
    address.push(dbData.primaryAddress);
  }
  if (!_.isEmpty(dbData.city)) {
    address.push(dbData.city);
  }
  if (!_.isEmpty(dbData.zipCode)) {
    address.push(dbData.zipCode);
  }
  // remove falsy (null, undefined) values from array, create address string and remove whitespace
  return _.trim(_.join(_.compact(address), ', ')); 
};
module.exports = {
  findAddress: async (req, res) => {
    try {
      const address = getCompleteAddress(req.body);
      const countyKey: any = 'administrative_area_level_2';
      const stateKey: any = 'administrative_area_level_1';
      const postalKey: any = 'postal_code';
      const cityKey: any = 'locality';
      let stateValue: any = '';
      let countyValue: any = '';
      let postalValue: any = '';
      let cityValue: any = '';
      let jurisdiction: any = '';
      let jsonData: any = {};
      const GOOGLE_MAP_API_KEY: any = 'Enter google API key Here'; //enter your own google map geocode api key
      const response = await client.geocode({
        params: {
          address: address,
          key: GOOGLE_MAP_API_KEY,
        },
        timeout: 3000, // milliseconds
      });
      const result = response?.data?.results[0]?.address_components;
      try {
        if (result && result.length > 0) {
          result.forEach(data => {
            if (Array.isArray(data.types)) {
              if (data.types.includes(countyKey)) {
                countyValue = data.long_name;
              }
              if (data.types.includes(stateKey)) {
                stateValue = data.long_name;
              }
              if (data.types.includes(postalKey)) {
                postalValue = data.long_name;
              }
              if (data.types.includes(cityKey)) {
                cityValue = data.long_name;
              }
            }
          });
        }
        let searchTerm =
          !_.isNil(countyValue) && countyValue.split(' ').length > 0
            ? countyValue.split(' ')[0]
            : undefined;
        if (
          searchTerm !== '' &&
          !_.isNil(searchTerm)
        ) {
          jsonData.county = countyValue;
          jsonData.jurisdiction = jurisdiction;
          jsonData.state = stateValue;
          jsonData.city = cityValue;
          jsonData.zip = postalValue;
        }
      } catch (error) {
        return {
          error: error,
          message: 'Google API unable to find the county.',
        };
      }
      res.status(200).send({
        result: jsonData,
      });
    } catch (e) {
      console.error('Error in /google-map-api', e);
      return res.status(400).send({
        error: "Error" + e
      });
    }
  },
};